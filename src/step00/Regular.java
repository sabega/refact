package step00;

public class Regular extends Category {

	@Override
	public double getCharge(int daysRented) {
		int result = 2;
		if (daysRented > 2)
			result += (daysRented - 2) * 1.5;
		return result;
	}
	
}
