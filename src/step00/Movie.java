package step00;

public class Movie {

  private String _title;
  private Category _priceCode;

  public Movie(String title, int priceCode) {
    _title = title;
    setPriceCode(priceCode);
  }

  public Category getPriceCode() {
    return _priceCode;
  }

  public void setPriceCode(int priceCode) {
    switch (priceCode) {
      case (0):
        setPriceCode(new Regular());
        break;
      case(1):
        setPriceCode(new New_release());
        break;
      case(2):
        setPriceCode(new Children());
        break;
      default:
        throw(new IllegalArgumentException("Expected different number"));
    }
  }

  public String getTitle() {
    return _title;
  }

  public double getCharge(int daysRented) {
    return getPriceCode().getCharge(daysRented);
  }

  public int getFrequentRenterPoints(int dasyRented) {
    return getPriceCode().getFrequentRenterPoints(dasyRented);
  }

  /**
   * @param _priceCode the _priceCode to set
   */
  public void setPriceCode(Category _priceCode) {
    this._priceCode = _priceCode;
  }
}
